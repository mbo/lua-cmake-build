# Lua CMake Build

## About
There are miscellaneous batch files and tutorials on how to build Lua for Windows.  This CMake script simplifies the steps across platforms, but is primarily useful for Windows users since the Makefile included in the Lua source code does not target NMake or Visual Studio.

## Requirements
1. Lua source code.
2. CMake (Visual Studio installer has an option for "C++ CMake tools for Windows", or download from cmake.org)
3. Any build environment, but I recommend [Visual Studio Build Tools](https://aka.ms/vs/17/release/vs_BuildTools.exe) for Windows.

## Usage
1. Download Lua source code to preferred location. (For example: c:/src/lua-5.4.6)
2. Git clone https://git.disroot.org/mbo/lua-cmake-build.git or download the script and configuration file to c:/src/lua-cmake-build
3. Configure lua-config.cmake with the path to the Lua src directory, and set the desired output names for the binaries and libraries.
4. Open build environment terminal (for example "x64 Native Tools Command Prompt for VS 2022")
5. Change directory to c:/src/lua-cmake-build
6. mkdir build
7. cd build
8. cmake -DCMAKE_BUILD_TYPE=Release -G "NMake Makefiles" ..
9. nmake install
10. copy lua54.exe, lua54.dll, lua54_static.lib, luac54.exe to desired location.

### Notes
 - This was tested with Lua versions:
	 - 5.1.5
	 - 5.3.6
	 - 5.4.6
 - Toolchains: 
	 - Visual Studio 2022 Build Tools
	 - MSYS2 UCRT64

