### Full path to lua/src.
set(LUA_SRC_DIR "u:/src/lua-5.4.6/src")

### Desired names for lua.exe, luac.exe, lua.dll, and lua_static.lib
set(LUA_BINARY_NAME "lua54")
set(LUAC_BINARY_NAME "luac54")
set(LUA_LIBRARY_NAME "lua54")
set(LUA_STATIC_NAME "lua54_static")

## Note:
## Some platforms may want liblua54.dll instead of lua54.dll for example.
## If this is the case, remove PREFIX "" statements from the CMakeList.txt
